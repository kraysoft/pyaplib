"""

@author: rghosh@kraysoft.in

"""


import time
from datetime import date
from dateutil.relativedelta import relativedelta
import logging
import requests
import json
import numpy as np
import pandas as pd
from pandas.tseries.offsets import CustomBusinessDay
from pandas.tseries.holiday import USFederalHolidayCalendar


class ArcticPond(object):
    """
    This class is used to get data from data base or resampling the data
    """
    def __init__(self, host, port, apikey, freq='T'):
        self.logger = logging.getLogger(__name__)#logging file
        self.host = host
        self.port = port
        self.apikey = apikey
        self.freq = freq
        self.__check_connection(host=self.host, port=self.port, apikey=self.apikey)

    def __check_connection(self, host, port, apikey):
        """
        Check the host and port are properly connected or not
        ------------------------------------------------------
        Paramter:
        ----------
            Host : str
                host name of server
            Port : str
                port name of server
        """
        url = 'http://'+ host +':' + port + '/' + "API?key="+str(apikey)


        try:
            res_get = requests.get(url)
            if res_get.status_code is 200:
                self.logger.debug('Good Host(%s) and Port(%s) and Valid API key(%s)' %(host, port, apikey))
            else:
                raise Exception('Invalid API')
        except Exception as error:
            self.logger.warning('WARNING: Invalid Host or Port.')
            print('WARNING!!!!\
                  Invalid Host or Port or APIKEY.\
                  Please check...' + repr(error))
            #print('error':repr(error))


    def get(self, symbol, start_date=None, end_date=None, column=None):
        """
        Return the data for symbol.

        Parameters
        -----------------
        symbol : str
            symbol name to get data.
        start_date : 'str'
            start date from when to get data.
        end_date : 'str'
            end date to get data.
        column : 'str' eg:'close,volume'
            columns name

        Return:
        --------
        data : series/dataframe

        """
        _Host = self.host
        _Port = self.port
        _API = self.apikey

        try:
            if start_date is None and end_date is None:
                today = date.today()
                d = today - relativedelta(months=1)
                start_date = d.strftime('%Y-%m-%d')
                end_date = today.strftime('%Y-%m-%d')
            elif start_date is None:
                d = pd.to_datetime(end_date) - relativedelta(months=1)
                start_date = d.strftime('%Y-%m-%d')
            elif end_date is None:
                today = date.today()
                end_date = today.strftime('%Y-%m-%d')
            if column is None:
                column = 'open,high,low,close,volume'
            else:
                pass

            url = 'http://'+ _Host +':'+ _Port +'/Data?apikey='+_API+'&symbol='+symbol+\
            '&start='+start_date+'&end='+end_date+'&col='+column

            res_get = requests.get(url)
            time.sleep(1)
            if res_get.status_code == 200:
                res = json.loads((res_get.content).decode('utf-8'))
                if list(res['timeseries'].keys()) == ['message']:
                    raise Exception(str(*res.values()))

                meta = res['meta']
                self.logger.info('INFO: got metadata for symbol %s' %(symbol))

                df = pd.DataFrame.from_dict(res['timeseries'])
                df['date']=df['date'].astype('datetime64[ns]')
                data = df.set_index(['date'])
                data = data.sort_index()
                self.logger.info('INFO: got data for symbol %s from %s to %s'\
                                 %(symbol, start_date, end_date))
                return meta, data
            else:
                self.logger.error('ERROR: status code is not 200')
                return {"status" : res_get.status_code}
        except Exception as error:
            self.logger.error('ERROR: %s' %(repr(error)))
            raise


    def resample(self, data, offset='D'):
        """
        Resampling data into Daily, Weekly, Monthly without setting the calendar
        ------------------------------------------------------------------------
        Parameters
        -----------------
        data : DataFrame
            data to transform daily
        offset : strt, optional
            offset value eg: 'D'
        Return:
        --------
        data : dataFrame
        """
        BMTH_US = CustomBusinessDay(calendar=USFederalHolidayCalendar())

        col = data.columns.tolist()
        map_1 = {"high":np.max, "close":'last', "open":'first', "low":np.min, "volume":sum}
        j = {}
        for i in col:
            j[i] = map_1[i]
        df_r = data.resample(offset).agg(j)
        new_df = df_r.asfreq(freq=BMTH_US)
        return new_df


    def heiken_ashi(self, data):
        """
        Transform all Data into heikin_ashi candle stick
        -------------------------------------------------
        Parameters
        -----------------
        data : DataFrame
            data to transform heikin_ashi data
        Return:
        --------
        data : dataFrame
        """

        col = data.columns.tolist()
        column = ['close', 'high', 'low', 'open']
        try:
            if set(col).intersection(set(column)) == set(column):
                Open = data['open']
                High = data['high']
                Low = data['low']
                Close = data['close']


                close = (Open + High + Low + Close) / 4
                _open = (Open.shift(1) + Close.shift(1)) / 2
                high = data['high']
                low = data['low']

                heiken_data = pd.concat([_open, high, low, close], axis=1)
                heiken_data.columns = ['open', 'high', 'low', 'close']
                return heiken_data
        except:
            raise
#EOF

