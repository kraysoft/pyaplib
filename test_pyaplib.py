#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 16:23:40 2018

@author: rghosh@kraysoft.in
"""


from pyaplib import ArcticPond

from config import HOST, PORT


sym = ['USDJPY', 'AAPL', 'QQQ', 'ES', '$ADDA']
start = ['01/01/2009','2014-01-01', '2019/01/01']
end= ['2009/02/28', '01-12-2015']
col = ['close', 'close,volume', 'high,low,close', 'open,high,low,close', 'open,high,low,close,volume']


x = ArcticPond(host=HOST, port=PORT)

print("Start test case 1")
df = x.get(symbol=sym[0])
print("End test case 1 with param %s" %(sym[0]))


print("Start test case 2")
df=x.get(symbol=sym[0], start_date=start[0])
print("End test case 2 with param %s %s" %(sym[0], start[0]))


print("Start test case 3")
df=x.get(symbol=sym[1], start_date=start[0], end_date=end[0])
print("End test case 3 with param %s, %s, %s" %(sym[1], start[0], end[0]))


print("Start test case 4")
df = x.get(symbol=sym[2], column=col[3])
print("End test case 4 with param %s, %s" %(sym[2], col[3]))


print("Start test case 5")
df=x.get(symbol=sym[1], start_date=start[1], end_date=end[1], column=col[0])
print("End test case 5 with param %s, %s, %s, %s" %(sym[1], start[1], end[1], col[0]))


print("Start test case 6")
df=x.get(symbol=sym[3], end_date=end[1], column=col[4])
print("End test case 6 with param %s, %s, %s" %(sym[3], end[1], col[4]))


print("Start test case 7")
df=x.get(symbol=sym[4], start_date=start[0],end_date=end[1], column=col[1])
print("End test case 7 with param %s, %s, %s, %s" %(sym[4], start[0], end[1], col[1]))


print("Start test case 8")
df=x.get(symbol=sym[3], start_date=start[2], column=col[2])
print("End test case 8 with param %s, %s, %s" %(sym[3], start[2], col[2]))


