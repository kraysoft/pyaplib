#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 16:23:40 2018

@author: root
"""

from pyaplib import ArcticPond
from config import HOST, PORT, API_KEY
import io


sym = ['USDJPY', 'AAPL', 'QQQ', 'ES', '$ADDA']
start = ['01/01/2009','2014-01-01', '2019/01/01']
end= ['2009/02/28', '01-12-2015']
col = ['close', 'close,volume', 'high,low,close', 'open,high,low,close', 'open,high,low,close,volume']


arctic_pond = ArcticPond(host=HOST, port=PORT, apikey=API_KEY)

print("Start test case 1\n")
meta_1, data_1 = arctic_pond.get(symbol=sym[0])
print("End test case 1 with param %s\n" %(sym[0]))
print("Meta : %s\n" %meta_1)
buf_1 = io.StringIO()
data_1.info(buf=buf_1)
print("Data : %s\n" %buf_1.getvalue())


print("Start test case 2\n")
meta_2, data_2 = arctic_pond.get(symbol=sym[0], start_date=start[0])
print("End test case 2 with param %s %s\n" %(sym[0], start[0]))
print("Meta : %s\n" %meta_2)
buf_2 = io.StringIO()
data_2.info(buf=buf_2)
print("Data : %s\n" %buf_2.getvalue())


print("Start test case 3\n")
meta_3, data_3 = arctic_pond.get(symbol=sym[1], start_date=start[0], end_date=end[0])
print("End test case 3 with param %s, %s, %s\n" %(sym[1], start[0], end[0]))
print("Meta : %s\n" %meta_3)
buf_3 = io.StringIO()
data_3.info(buf=buf_3)
print("Data : %s\n" %buf_3.getvalue())


print("Start test case 4\n")
meta_4, data_4 = arctic_pond.get(symbol=sym[2], column=col[3])
print("End test case 4 with param %s, %s\n" %(sym[2], col[3]))
print("Meta : %s\n" %meta_4)
buf_4 = io.StringIO()
data_4.info(buf=buf_4)
print("Data : %s\n" %buf_4.getvalue())


print("Start test case 5\n")
meta_5, data_5 = arctic_pond.get(symbol=sym[1], start_date=start[1], end_date=end[1], column=col[0])
print("End test case 5 with param %s, %s, %s, %s\n" %(sym[1], start[1], end[1], col[0]))
print("Meta : %s\n" %meta_5)
buf_5 = io.StringIO()
data_5.info(buf=buf_5)
print("Data : %s\n" %buf_5.getvalue())


print("Start test case 6\n")
meta_6, data_6 = arctic_pond.get(symbol=sym[3], end_date=end[1], column=col[4])
print("End test case 6 with param %s, %s, %s\n" %(sym[3], end[1], col[4]))
print("Meta : %s\n" %meta_6)
buf_6 = io.StringIO()
data_6.info(buf=buf_6)
print("Data : %s\n" %buf_6.getvalue())


print("Start test case 7\n")
meta_7, data_7 = arctic_pond.get(symbol=sym[4], start_date=start[0],end_date=end[1], column=col[1])
print("End test case 7 with param %s, %s, %s, %s\n" %(sym[4], start[0], end[1], col[1]))
print("Meta : %s\n" %meta_7)
buf_7 = io.StringIO()
data_7.info(buf=buf_7)
print("Data : %s\n" %buf_7.getvalue())


print("Start test case 8\n")
meta_8, data_8 = arctic_pond.get(symbol=sym[3], start_date=start[2], column=col[2])
print("End test case 8 with param %s, %s, %s\n" %(sym[3], start[2], col[2]))
print("Meta : %s\n" %meta_8)
buf_8 = io.StringIO()
data_8.info(buf=buf_8)
print("Data : %s\n" %buf_8.getvalue())

