# PyAPlib - python Arctic Pond library
## The client api for accessing Arctic Pond data using python.

Arctic Pond is a cloud database consisting of Time Series data for various financial instruments.
There are 5 libraries at the moment.
1. US Equity 
    * 3000+ listed stocks
    * Top 200 ETFs
2. Major Forex
3. Major Futures
4. Global Indices.

## Quick Start

Activate a Python 3 environment, e.g. conda activate py3stable

### Install

pip install git+https://gitlab.com/kraysoft/pyaplib.git


### Basic Usage
TODO:

## API KEY

You will need to get a API key to use **pyaplib**.
Get your api key by registering [here](https://goo.gl/forms/hWsjYeACtzd6xDq82) or send an email to
info@kraysoft.in requesting an API key.
