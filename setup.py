from setuptools import setup,find_packages

setup(
    name='pyaplib',
    version='0.1.0',
    author='Riyanka Ghosh',
    author_email='rghosh@kraysoft.in',
    scripts=['pyaplib.py'],
    packages=find_packages(),
    license='LICENSE.txt',
    include_package_data=True,
    description='Python API for ArcticPond',
    long_description=open('README.md', "r").read(),
    install_requires=[
        "pandas",
        "requests",
    ],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent"
    ],
)
